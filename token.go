package tokencli

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"golang.org/x/crypto/ssh/terminal"
)

// Token general struct
type Token struct {
	User string
	URL  string
}

// Error from token endpoint
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Reason  string `json:"reason"`
}

// Get a fresh token by prompting password
func (t *Token) Get() (string, error) {
	var error *Error
	fmt.Printf("Trying to fetch a new token by user id %s\n", t.User)
	fmt.Printf("Enter AD credentials to get user token:\n")
	bpass, err := terminal.ReadPassword(0)
	if err != nil {
		return "", fmt.Errorf("Could not read password, %s", err)
	}
	pass := string(bpass)
	req, err := http.NewRequest("GET", t.URL, nil)
	if err != nil {
		return "", fmt.Errorf("Could not make a request, %s", err)
	}
	req.SetBasicAuth(strings.TrimSpace(t.User), strings.TrimSpace(pass))
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	if res.StatusCode >= 400 {
		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return "", err
		}
		json.Unmarshal(b, &error)
		return "", fmt.Errorf("%d - Message: %s, Reason: %s",
			error.Code, error.Message, error.Reason)
	}
	token, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	return string(token), nil
}
